<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="UTF-8" />
	<title>Document</title>
</head>
<body>
	<h1>서버시간 : ${serverTime }</h1>
	<hr>
	${user }님 반갑습니다.<br>
	<c:if test="${user!='anonymousUser' }">
		<a href="logout">로그아웃</a><br>
	</c:if>
	<a href="admin">최고 관리자 페이지</a><br>
	<a href="dba">디비 관리자 페이지</a><br>
</body>
</html>